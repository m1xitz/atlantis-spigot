package de.m0xitz.atlantis.spigot.listener;

import de.m0xitz.atlantis.spigot.Atlantis;
import de.m0xitz.atlantis.spigot.user.User;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {

    private final Atlantis atlantis;

    public PlayerListener( Atlantis atlantis ) {
        this.atlantis = atlantis;
    }

    @EventHandler
    public void handleJoin( PlayerJoinEvent event ) {
        Player player = event.getPlayer( );
        User user = this.atlantis.getUserManager( ).registerUser( player );

        this.atlantis.getUuidFetcher().putToCache( user );
        event.setJoinMessage( null );
    }

    @EventHandler
    public void handleQuit( PlayerQuitEvent event ) {
        Player player = event.getPlayer( );
        User user = this.atlantis.getUserManager( ).unregisterUser( player );

        event.setQuitMessage( null );
    }
}
