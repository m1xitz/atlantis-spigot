package de.m0xitz.atlantis.spigot;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import de.m0xitz.atlantis.spigot.command.TestCommand;
import de.m0xitz.atlantis.spigot.listener.PlayerListener;
import de.m0xitz.atlantis.spigot.manager.CommandManager;
import de.m0xitz.atlantis.spigot.manager.LanguageManager;
import de.m0xitz.atlantis.spigot.manager.UserManager;
import de.m0xitz.atlantis.spigot.misc.UuidFetcher;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

@Getter
public class Atlantis extends JavaPlugin {

    @Getter
    private static Atlantis instance;

    private ProtocolManager protocolManager;
    private UuidFetcher uuidFetcher;

    private CommandManager commandManager;
    private LanguageManager languageManager;
    private UserManager userManager;

    @Override
    public void onEnable( ) {
        Atlantis.instance = this;

        this.protocolManager = ProtocolLibrary.getProtocolManager( );
        this.uuidFetcher = new UuidFetcher( );

        this.commandManager = new CommandManager( this );
        this.languageManager = new LanguageManager( this );
        this.userManager = new UserManager( this );

        this.registerCommands( );
        this.registerListeners( );
    }


    private void registerCommands( ) {
        this.commandManager.register( new TestCommand( ) );
    }

    private void registerListeners( ) {
        this.getServer( ).getPluginManager( ).registerEvents( new PlayerListener( this ), this );
    }
}
