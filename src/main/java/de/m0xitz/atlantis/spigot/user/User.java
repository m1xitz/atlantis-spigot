package de.m0xitz.atlantis.spigot.user;

import com.comphenix.packetwrapper.AbstractPacket;
import com.comphenix.packetwrapper.WrapperPlayServerChat;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.mojang.authlib.properties.Property;
import de.m0xitz.atlantis.spigot.Atlantis;
import de.m0xitz.atlantis.spigot.misc.LanguageType;
import lombok.Getter;
import net.md_5.bungee.api.ChatMessageType;
import net.minecraft.server.v1_8_R3.Packet;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.util.UUID;

@Getter
public class User {

    private final Atlantis atlantis;

    private final Player player;
    private final CraftPlayer craftPlayer;

    private final UUID realUuid;
    private final String realName;

    private LanguageType languageType;

    public User( Player player ) {
        this.atlantis = Atlantis.getInstance( );

        this.player = player;
        this.craftPlayer = ( CraftPlayer ) player;

        this.realUuid = player.getUniqueId( );
        this.realName = player.getName( );

        this.languageType = LanguageType.GERMAN;
    }

    public String getName( ) {
        return this.craftPlayer.getProfile( ).getName( );
    }

    public UUID getUuid( ) {
        return this.craftPlayer.getProfile( ).getId( );
    }

    public Property getSkin( ) {
        return this.craftPlayer.getProfile( ).getProperties( ).get( "textures" ).iterator( ).next( );
    }

    public void sendMessage( ChatMessageType chatMessageType, String key, Object... objects ) {
        String message = this.translate( key, objects );
        WrapperPlayServerChat chatPacket = new WrapperPlayServerChat( );
        switch ( chatMessageType ) {
            case SYSTEM:
                message = " §b§lAtlantis §8§l┃ §7" + message;
                break;
            case ACTION_BAR:
                chatPacket.setPosition( ( byte ) 2 );
                break;
        }
        chatPacket.setMessage( WrappedChatComponent.fromText( message ) );
        this.sendPacket( chatPacket );
    }

    public void sendMessage( String key, Object... objects ) {
        this.sendMessage( ChatMessageType.SYSTEM, key, objects );
    }

    public String translate( String key, Object... objects ) {
        String message = this.atlantis.getLanguageManager( ).getString( this.languageType, key );
        return String.format( message, objects );
    }

    public void sendPacket( Packet<?> packet ) {
        this.craftPlayer.getHandle( ).playerConnection.sendPacket( packet );
    }

    public void sendPacket( PacketContainer packetContainer ) {
        try {
            this.atlantis.getProtocolManager( ).sendServerPacket( this.player, packetContainer );
        } catch ( InvocationTargetException ex ) {
            ex.printStackTrace( );
        }
    }

    public void sendPacket( AbstractPacket abstractPacket ) {
        abstractPacket.sendPacket( this.player );
    }

    public void disconnect( String message ) {
        this.player.kickPlayer( message );
    }

    public int getPing( ) {
        return this.craftPlayer.getHandle( ).ping;
    }

    public Location getLocation( ) {
        return this.player.getLocation( );
    }

    public void teleport( Location location ) {
        this.player.teleport( location );
    }

    public void teleport( Entity entity ) {
        this.player.teleport( entity );
    }

    public void teleport( int x, int y, int z ) {
        this.teleport( new Location( this.player.getWorld( ), x, y, z ) );
    }

    public void syncSettings( ) {

    }
}
