package de.m0xitz.atlantis.spigot.manager;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import de.m0xitz.atlantis.spigot.Atlantis;
import de.m0xitz.atlantis.spigot.user.User;
import lombok.Getter;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Getter
public class UserManager {

    private final Atlantis atlantis;
    private final List<User> users;
    private final HashMap<UUID, User> userHashMap;

    public UserManager( Atlantis atlantis ) {
        this.atlantis = atlantis;
        this.users = Lists.newArrayList( );
        this.userHashMap = Maps.newHashMap( );
    }

    public User registerUser( Player player ) {
        if ( this.userHashMap.containsKey( player.getUniqueId( ) ) )
            return this.userHashMap.get( player.getUniqueId( ) );

        User user = new User( player );

        this.users.add( user );
        this.userHashMap.put( player.getUniqueId( ), user );
        return user;
    }

    public User unregisterUser( Player player ) {
        if ( !this.userHashMap.containsKey( player.getUniqueId( ) ) )
            return null;

        User user = this.userHashMap.get( player.getUniqueId( ) );
        this.users.remove( user );
        return this.userHashMap.remove( player.getUniqueId( ) );
    }

    public User getUser( Player player ) {
        return this.userHashMap.getOrDefault( player.getUniqueId( ), registerUser( player ) );
    }

    public User getUserExact( String name ) {
        User user = null;
        for ( User allUser : this.users ) {
            if ( !allUser.getName( ).equals( name ) )
                continue;
            user = allUser;
        }
        if ( user == null )
            return null;
        return user;
    }

    public User getUser( String name ) {
        if ( name == null )
            return null;
        User found = this.getUserExact( name );
        if ( found != null ) {
            return found;
        }
        String lowerName = name.toLowerCase( );
        int delta = 2147483647;
        for ( User user : this.users ) {
            if ( user.getName( ).toLowerCase( ).startsWith( lowerName ) ) {
                int curDelta = Math.abs( user.getName( ).length( ) - lowerName.length( ) );
                if ( curDelta < delta ) {
                    found = user;
                    delta = curDelta;
                }
                if ( curDelta == 0 ) {
                    break;
                }
            }
        }
        return found;
    }

    public User getUser( UUID uuid ) {
        return this.userHashMap.getOrDefault( uuid, registerUser( this.atlantis.getServer( ).getPlayer( uuid ) ) );
    }

    public int getOnlineSize( ) {
        return this.users.size( );
    }

    public List<User> matchUser( String targetName ) {
        List<User> matchedUsers = Lists.newArrayList( );
        if ( targetName == null )
            return matchedUsers;
        for ( User user : this.users ) {
            String userName = user.getName( );
            if ( targetName.equalsIgnoreCase( userName ) ) {
                matchedUsers.clear( );
                matchedUsers.add( user );
                break;
            }

            if ( userName.toLowerCase( ).contains( targetName.toLowerCase( ) ) ) {
                matchedUsers.add( user );
            }
        }
        return matchedUsers;
    }

    public boolean isOnline( UUID uuid ) {
        return this.userHashMap.containsKey( uuid );
    }

}
