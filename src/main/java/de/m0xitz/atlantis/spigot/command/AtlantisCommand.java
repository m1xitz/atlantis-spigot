package de.m0xitz.atlantis.spigot.command;

import de.m0xitz.atlantis.spigot.Atlantis;
import de.m0xitz.atlantis.spigot.user.User;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;

@Getter
@Setter
public abstract class AtlantisCommand extends BukkitCommand {

    private final Atlantis atlantis;
    private boolean console;

    public AtlantisCommand( String name ) {
        super( name );

        this.atlantis = Atlantis.getInstance( );
        this.console = false;

        this.setPermission( "atlantis.command." + name );
    }

    @Override
    public boolean execute( CommandSender commandSender, String command, String[] strings ) {
        if ( !( commandSender instanceof Player ) ) {
            if ( this.console ) {
                this.onConsole( commandSender, strings );
                return true;
            }
            commandSender.sendMessage( "§cDieser Command ist nur für Spieler gedacht§8." );
            return true;
        }
        User user = this.atlantis.getUserManager( ).getUser( ( Player ) commandSender );
        if ( !testPermission( user ) )
            return true;
        this.onUser( user, strings );
        return true;
    }

    public abstract void onUser( User user, String[] strings );

    public void onConsole( CommandSender commandSender, String[] strings ) {
    }

    public boolean testPermission( User user ) {
        if ( !this.testPermissionSilent( user.getPlayer( ) ) ) {
            user.sendMessage( "message.no-permission" );
            return false;
        }
        return true;
    }
}
