package de.m0xitz.atlantis.spigot.manager;

import com.google.common.collect.Maps;
import de.m0xitz.atlantis.spigot.Atlantis;
import de.m0xitz.atlantis.spigot.misc.LanguageType;
import lombok.Getter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

@Getter
public class LanguageManager {

    private final Atlantis atlantis;

    private final Map<String, String> gerLanguage;
    private final Map<String, String> engLanguage;

    public LanguageManager( Atlantis atlantis ) {
        this.atlantis = atlantis;

        this.gerLanguage = Maps.newConcurrentMap( );
        this.engLanguage = Maps.newConcurrentMap( );

        this.loadMessages( );
    }

    public void loadMessages( ) {
        this.gerLanguage.clear( );
        this.engLanguage.clear( );

        Map<String, String> defaults = Maps.newHashMap( );
        defaults.put( "message.no-permission", "Message not set." );

        this.load( defaults, this.gerLanguage, "german" );
        this.load( defaults, this.engLanguage, "english" );
    }

    public String getString( LanguageType languageType, String key ) {
        String message = "§cMessage §8(§e" + key + "§8) §cnot found§8.";

        switch ( languageType ) {
            case GERMAN:
                message = this.gerLanguage.get( key );
                break;
            case ENGLISH:
                message = this.engLanguage.get( key );
                break;
        }
        return message.replace( "&", "§" );
    }

    private void load( Map<String, String> defaultValues, Map<String, String> languageMap, String fileName ) {
        Properties properties = new Properties( );
        FileInputStream fileInputStream = null;
        try {
            File folder = new File( "languages/Atlantis" );
            if ( !folder.exists( ) )
                folder.mkdirs( );

            File file = new File( folder.getPath( ), fileName + ".properties" );
            if ( !file.exists( ) ) {
                file.createNewFile( );
                this.loadDefaults( defaultValues, properties, file );
            } else {
                fileInputStream = new FileInputStream( file );
                properties.load( fileInputStream );
            }
        } catch ( IOException ex ) {
            ex.printStackTrace( );
        } finally {
            try {
                assert fileInputStream != null;
                fileInputStream.close( );
            } catch ( IOException ex ) {
                ex.printStackTrace( );
            } catch ( NullPointerException ignored ) {
            }
        }

        for ( Object key : properties.keySet( ) ) {
            languageMap.put( ( String ) key, properties.getProperty( ( String ) key ) );
        }
    }

    private void loadDefaults( Map<String, String> defaultValues, Properties properties, File file ) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream( file );
            properties.putAll( defaultValues );
            properties.save( fileOutputStream, "" );
        } catch ( IOException ex ) {
            ex.printStackTrace( );
        } finally {
            try {
                fileOutputStream.close( );
            } catch ( IOException ex ) {
                ex.printStackTrace( );
            }
        }
    }
}
