package de.m0xitz.atlantis.spigot.misc;

import lombok.Getter;

@Getter
public enum LanguageType {

    GERMAN( "German" ), ENGLISH( "English" );

    private String betterName;

    LanguageType( String betterName ) {
        this.betterName = betterName;
    }

    @Override
    public String toString( ) {
        return this.betterName;
    }
}
