package de.m0xitz.atlantis.spigot.manager;

import com.google.common.collect.Lists;
import de.m0xitz.atlantis.spigot.Atlantis;
import de.m0xitz.atlantis.spigot.command.AtlantisCommand;
import lombok.Getter;
import org.bukkit.command.CommandMap;

import java.lang.reflect.Field;
import java.util.List;

@Getter
public class CommandManager {

    private final Atlantis atlantis;
    private final List<AtlantisCommand> commands;

    public CommandManager( Atlantis atlantis ) {
        this.atlantis = atlantis;
        this.commands = Lists.newArrayList( );
    }

    public void register( AtlantisCommand atlantisCommand ) {
        CommandMap commandMap;

        try {
            Field field = this.atlantis.getServer( ).getClass( ).getDeclaredField( "commandMap" );
            field.setAccessible( true );
            commandMap = ( CommandMap ) field.get( this.atlantis.getServer( ) );
            field.setAccessible( false );
        } catch ( IllegalAccessException | NoSuchFieldException ex ) {
            ex.printStackTrace( );
            return;
        }

        commandMap.register( this.atlantis.getName( ), atlantisCommand );
        this.commands.add( atlantisCommand );
    }
}
