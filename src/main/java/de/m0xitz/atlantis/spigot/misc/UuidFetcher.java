package de.m0xitz.atlantis.spigot.misc;

import com.google.common.collect.Maps;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mojang.authlib.properties.Property;
import de.m0xitz.atlantis.spigot.user.User;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class UuidFetcher {

    private final ExecutorService pool;

    private final String uuidUrl;
    private final String nameUrl;
    private final String skinUrl;

    private HashMap<String, UUID> uuidCache;
    private HashMap<UUID, String> nameCache;

    private HashMap<UUID, Property> skinCache;

    public UuidFetcher( ) {
        this.pool = Executors.newFixedThreadPool( 10 );

        this.uuidUrl = "https://api.mojang.com/users/profiles/minecraft/%s";
        this.nameUrl = "https://api.mojang.com/user/profiles/%s/names";
        this.skinUrl = "https://sessionserver.mojang.com/session/minecraft/profile/%s?unsigned=false";

        this.uuidCache = Maps.newHashMap( );
        this.nameCache = Maps.newHashMap( );
        this.skinCache = Maps.newHashMap( );
    }

    public String getName( UUID uuid ) {
        if ( this.nameCache.containsKey( uuid ) ) {
            return this.nameCache.get( uuid );
        }
        String name = null;

        HttpURLConnection connection = null;
        InputStreamReader inputStreamReader = null;
        try {
            connection = ( HttpURLConnection ) new URL( String.format( this.nameUrl, this.fromUUID( uuid ) ) ).openConnection( );
            inputStreamReader = new InputStreamReader( connection.getInputStream( ) );

            JsonArray jsonArray = ( JsonArray ) new JsonParser( ).parse( inputStreamReader );
            JsonObject jsonObject = jsonArray.get( jsonArray.size( ) - 1 ).getAsJsonObject( );

            name = jsonObject.get( "name" ).getAsString( );
        } catch ( IOException ex ) {
            ex.printStackTrace( );
        } catch ( ClassCastException ignored ) {
        } finally {
            try {
                connection.disconnect( );
                inputStreamReader.close( );
            } catch ( IOException ex ) {
                ex.printStackTrace( );
            }
        }


        if ( name != null ) {
            this.nameCache.put( uuid, name );
            this.uuidCache.put( name, uuid );
        }
        return name;
    }

    public UUID getUuid( String name ) {
        name = name.toLowerCase( );
        if ( this.uuidCache.containsKey( name ) )
            return this.uuidCache.get( name );
        UUID uuid = null;

        HttpURLConnection connection = null;
        InputStreamReader inputStreamReader = null;
        try {
            connection = ( HttpURLConnection ) new URL( String.format( this.uuidUrl, name ) ).openConnection( );
            inputStreamReader = new InputStreamReader( connection.getInputStream( ) );

            JsonObject jsonObject = ( JsonObject ) new JsonParser( ).parse( inputStreamReader );

            uuid = this.fromString( jsonObject.get( "id" ).getAsString( ) );
            name = jsonObject.get( "name" ).getAsString( );

        } catch ( IOException ex ) {
            ex.printStackTrace( );
        } catch ( ClassCastException ignored ) {
        } finally {
            try {
                connection.disconnect( );
                inputStreamReader.close( );
            } catch ( IOException ex ) {
                ex.printStackTrace( );
            }
        }

        if ( uuid != null ) {
            this.uuidCache.put( name, uuid );
            this.nameCache.put( uuid, name );
        }
        return uuid;
    }

    public Property getSkin( UUID uuid ) {
        if ( this.skinCache.containsKey( uuid ) )
            return this.skinCache.get( uuid );
        String value = null;
        String signature = null;

        HttpURLConnection connection = null;
        InputStreamReader inputStreamReader = null;
        try {
            connection = ( HttpURLConnection ) new URL( String.format( this.skinUrl, this.fromUUID( uuid ) ) ).openConnection( );
            inputStreamReader = new InputStreamReader( connection.getInputStream( ) );

            JsonObject jsonObject = ( JsonObject ) new JsonParser( ).parse( inputStreamReader );
            JsonObject skinObject = jsonObject.get( "properties" ).getAsJsonArray( ).get( 0 ).getAsJsonObject( );

            value = skinObject.get( "value" ).getAsString( );
            signature = skinObject.get( "signature" ).getAsString( );
        } catch ( IOException ex ) {
            ex.printStackTrace( );
        } catch ( ClassCastException ignored ) {
        } finally {
            try {
                connection.disconnect( );
                inputStreamReader.close( );
            } catch ( IOException ex ) {
                ex.printStackTrace( );
            }
        }

        if ( value == null || signature == null )
            return null;

        Property property = new Property( "textures", value, signature );
        this.skinCache.put( uuid, property );
        return property;
    }

    public void getName( UUID uuid, Consumer<String> consumer ) {
        this.pool.execute( ( ) -> consumer.accept( this.getName( uuid ) ) );
    }

    public void getUuid( String name, Consumer<UUID> consumer ) {
        this.pool.execute( ( ) -> consumer.accept( this.getUuid( name ) ) );
    }

    public void getSkin( UUID uuid, Consumer<Property> consumer ) {
        this.pool.execute( ( ) -> consumer.accept( this.getSkin( uuid ) ) );
    }

    public void putToCache( User user ) {
        this.nameCache.put( user.getUuid( ), user.getName( ) );
        this.uuidCache.put( user.getName( ), user.getUuid( ) );
        this.skinCache.put( user.getUuid( ), user.getSkin( ) );
    }

    // From com.mojang.util.UUIDTypeAdapter
    public String fromUUID( UUID uuid ) {
        return uuid.toString( ).replace( "-", "" );
    }

    public UUID fromString( String value ) {
        return UUID.fromString( value.replaceFirst( "(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5" ) );
    }
}
